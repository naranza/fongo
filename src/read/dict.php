<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/struct/term.php';
require_once FONGO_DIR . '/read/files.php';

function fongo_read_dict(array $files): array
{
  $dict = fongo_read_files($files);
  foreach ($dict as $id => $term) {
    $dict[$id] = new fongo_term($term);
  }
  return $dict;
}
