<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/read/config.php';
require_once FONGO_DIR . '/core/compile.php';

function fongo_read_files(array $files): array
{
  $data = [];
  foreach ($files as $name) {
    $data = array_merge($data, fongo_read_config(fongo_compile($name)));
  }
  return $data;
}
