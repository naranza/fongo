<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once FONGO_DIR . '/core/path.php';

function fongo_compile(string $path): string
{
  $parts = explode('::', $path);
  if (isset($parts[1])) {
    $dir = fongo_path($parts[0]);
    $name = $parts[1];
  } else {
    $paths = fongo_path();
    $dir = reset($paths);
    $name = $parts[0];
  }
  if (empty($dir)) {
    $path = $name . '.php';
  } else {
    $path = $dir . '/' . $name . '.php';
  }
  return $path;
}
