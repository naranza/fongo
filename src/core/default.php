<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

function fongo_default(array $form, array $context = []): array
{
  $record = [];
  foreach ($form as $field => $data) {
    $record[$field] = $data->default;
    if (isset($context[$field])) {
      $record[$field] = $context[$field];
    }
  }
  return $record;
}

