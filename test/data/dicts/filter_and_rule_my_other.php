<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

return [
  'my_field' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/greater_than_10']
  ],
  'my_other_field' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/greater_than_10']
  ]
];

