<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

$config = [
  'my_field' => [
    'filters' => ['filters/trim', 'filters/intval'],
  ],
];
