<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

return [
  'dob_year' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int']
  ],
  'dob_date' => [
    'type' => 'group',
    'compose' => '{dob_year}-{dob_month}-{dob_day}',
    'rules' => ['rules/valid_date']
  ],
  'dob_month' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int']
  ],
  'dob_day' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int', 'rules/greater_than_10']
  ],
];
