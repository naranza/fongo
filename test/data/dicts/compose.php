<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

return [
  'day' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int']
  ],
  'year' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int']
  ],
  'date' => [
//    'type' => 'group',
    'compose' => '{year}-{month}-{day}',
    'rules' => ['rules/valid_date']
  ],
  'month' => [
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/is_int', 'rules/greater_than_10']
  ],
];
