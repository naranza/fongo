<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

return [
  'my_field' => [
    'type' => 'var',
    'compose' => '',
    'default' => null,
    'required' => false,
    'empty_allowed' => false,
    'empty_function' => [
      'function' => function ($value) {
        return '' === $value;
      }
    ],
    'missing_message' => 'My message field',
    'fill_source' => 'value',
    'fill_value' => 1234,
    'filters' => ['filters/trim', 'filters/intval'],
    'rules' => ['rules/greater_than_10']
  ],
];

