<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

$config = [
  'my_field' => [
    'rules' => ['rules/greater_than_10']
  ],
];
