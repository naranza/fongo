<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

return [
  'function' => 'in_array',
  'args' => ['context::options'],
  'message' => '',
  'code' => 'invalid-date-error-code'
];

