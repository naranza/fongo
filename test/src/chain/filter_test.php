<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

require_once FONGO_DIR . '/core/path.php';

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/chain/filter.php';
  }


  public function t_empty(test $t)
  {
    $filters = [];
    $t->wie = ['1', ''];
    $t->wig = fongo_chain_filter($filters, '1');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_php_function(test $t)
  {
    $filters = [
      ['function' => 'intval']
    ];
    $t->wie = [1, ''];
    $t->wig = fongo_chain_filter($filters, '1');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_multiple_function(test $t)
  {
    $filters = [
      ['function' => 'trim'],
      ['function' => 'strtoupper']
    ];
    $t->wie = ['JUSTIN', ''];
    $t->wig = fongo_chain_filter($filters, ' justin ');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_error(test $t)
  {
    $filters = [
      ['function' => 'trimss'],
      ['function' => 'intval']
    ];
    $t->wie = [' 1 ', 'Function trimss not callable'];
    $t->wig = fongo_chain_filter($filters, ' 1 ');
    $t->pass_if($t->wie === $t->wig);
  }

}
