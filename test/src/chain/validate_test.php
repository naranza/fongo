<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

require_once FONGO_DIR . '/core/path.php';

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/chain/validate.php';
    require_once FONGO_DIR . '/read/option.php';
  }


  public function t_multiple_with_middle_rule_invalid(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, false]
      ],
      'rule_greater_or_equal_than_1' => [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, true]
      ]
    ];

    $t->wie = ['Invalid value', 1];
    $t->wig = fongo_chain_validate($rules, 1);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_multiple_invalid(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, false]
      ]
    ];

    $t->wie = ['Invalid value', 1];
    $t->wig = fongo_chain_validate($rules, 1);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_multiple_valid(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, true]
      ]
    ];

    $t->wie = ['', ''];
    $t->wig = fongo_chain_validate($rules, 1);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_context_valid(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => ['context::min', true]
      ]
    ];
    $context = [
      'min' => 10,
      'max' => 10
    ];
    $t->wie = ['', ''];
    $t->wig = fongo_chain_validate($rules, 11, $context);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_context_invalid(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => ['context::min', true]
      ]
    ];
    $context = [
      'min' => 10,
      'max' => 10
    ];
    $t->wie = ['Invalid value', 1];
    $t->wig = fongo_chain_validate($rules, 8, $context);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_not_exists_context(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => ['context::test', true]
      ]
    ];
    $context = [
      'min' => 10,
      'max' => 10
    ];
    $t->wie = ['Invalid context', 1];
    $t->wig = fongo_chain_validate($rules, 11, $context);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_not_exists_context_2(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => ['context::', true]
      ]
    ];
    $context = [
      'min' => 10,
      'max' => 10
    ];
    $t->wie = ['Invalid context', 1];
    $t->wig = fongo_chain_validate($rules, 11, $context);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_message(test $t)
  {
    $rules = [
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, false],
        'message' => 'my custom message'
      ],
      ['function' => 'is_int']
    ];

    $t->wie = ['my custom message', 0];
    $t->wig = fongo_chain_validate($rules, 1);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_custom_code(test $t)
  {
    $rules = [
      ['function' => 'is_int'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, false],
        'message' => 'my custom message',
        'code' => 'my custom code'
      ]
    ];

    $t->wie = ['my custom message', 'my custom code'];
    $t->wig = fongo_chain_validate($rules, 1);
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_error(test $t)
  {
    $rules = [
      ['function' => 'is_intsss'],
      [
        'require' => FONGO_TEST_FUNC_DIR . '/rule/greater_than.php',
        'function' => 'fongo_test_rule_greater_than',
        'args' => [1, false],
        'message' => 'my custom message',
        'code' => 'my custom code'
      ]
    ];

    $t->wie = ['Function is_intsss not callable', 0];
    $t->wig = fongo_chain_validate($rules, '');
    $t->pass_if($t->wie === $t->wig);
  }

  public function t_enumerated(test $t)
  {
    $rules = [
      [
        'function' => 'in_array',
        'args' => ['context::options'],
      ]
    ];

    $t->wie = ['Invalid value', 0];
    $t->wig = fongo_chain_validate($rules, '1', ['options' => fongo_read_option(['options/yes_no', 'options/y_n'])]);
    $t->pass_if($t->wie === $t->wig);
  }

}
