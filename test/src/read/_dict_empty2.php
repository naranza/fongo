<?php
$config = [
  'test2' => [
    'type' => 'test',
    'compose' => 'test',
    'default' => 'test',
    'required' => false,
    'missing_message' => 'test',
    'fill_source' => 'test',
    'fill_value' => 'test',
    'filters' => ['test'],
    'rules' => ['test'],
    'options' => ['test'],
  ]
];
