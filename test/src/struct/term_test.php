<?php

/* =============================================================================
 * Naranza Fongo - Copyright (c) Andrea Davanzo - License MPL v2.0 - naranza.org
 * ========================================================================== */

declare(strict_types=1);

use bateo_test as test;

class bateo_testcase
{

  public function setup()
  {
    require_once FONGO_DIR . '/struct/term.php';
  }

  public function t_term(test $t)
  {
    $term = new fongo_term([
      'type' => 'var',
      'compose' => '',
      'default' => null,
      'required' => false,
      'empty_allowed' => false,
      'empty_function' => [
        'function' => function ($value) {
          return '' === $value;
        }
      ],
      'missing_message' => 'My message field',
      'fill_source' => 'value',
      'fill_value' => 1234,
      'filters' => ['filters/trim', 'filters/intval'],
      'rules' => ['rules/greater_than_10']
    ]);
    $t->pass_if($term instanceof fongo_term);
  }

  public function t_magic_set(test $t)
  {
    $t->wie = new exception('__set not allowed on fongo_term');
    $t->wig = new exception('__set not allowed on fongo_term');
    $term = new fongo_term([
      'invalid' => '',
      'type' => 'var',
      'compose' => '',
      'default' => null,
      'required' => false,
      'empty_allowed' => false,
      'empty_function' => [
        'function' => function ($value) {
          return '' === $value;
        }
      ],
      'missing_message' => 'My message field',
      'fill_source' => 'value',
      'fill_value' => 1234,
      'filters' => ['filters/trim', 'filters/intval'],
      'rules' => ['rules/greater_than_10']
    ]);
  }

  public function t_magic_get(test $t)
  {
    $t->wie = new exception('(invalid) __get not allowed on fongo_term');
    $term = new fongo_term([
      'type' => 'var',
      'compose' => '',
      'default' => null,
      'required' => false,
      'empty_allowed' => false,
      'empty_function' => [
        'function' => function ($value) {
          return '' === $value;
        }
      ],
      'missing_message' => 'My message field',
      'fill_source' => 'value',
      'fill_value' => 1234,
      'filters' => ['filters/trim', 'filters/intval'],
      'rules' => ['rules/greater_than_10']
    ]);
    echo $term->invalid;
  }
}
